import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-homepage',
	templateUrl: './homepage.component.html',
	styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent implements OnInit {
	project_id: any;
	constructor(private _route: ActivatedRoute) {
		if (localStorage.getItem('Authobj')) {
			this.project_id = JSON.parse(
				localStorage.getItem('Authobj')
			).project_id;
		}
	}

	ngOnInit(): void {}
}
