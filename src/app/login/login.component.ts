import { Component, OnInit } from '@angular/core';
import {
	FormBuilder,
	FormGroup,
	ReactiveFormsModule,
	Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ScrumdataService } from '../scrumdata.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
	feedback = '';
	form: FormGroup;
	submitted = false;
	ngOnInit() {
		// this.submitted = true;
		this.form = this.fb.group({
			email: [null, [Validators.required, Validators.email]],
			password: [
				null,
				Validators.compose([
					Validators.required,
					Validators.minLength(8),
					Validators.pattern(
						/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*([^a-zA-Z\d\s])).{8,}$/
					),
				]),
			],
			projname: [null, Validators.required],
		});
	}
	// scrumUserLoginData = new ScrumUserLoginData('', '', '');
	constructor(
		private fb: FormBuilder,
		private _scrumdataService: ScrumdataService,
		private _router: Router
	) {}
	// convenience getter for easy access to form fields
	get f() {
		return this.form.controls;
	}
	formSubmit() {
		this.submitted = true;
		this._scrumdataService.login(this.form.value).subscribe(
			(data) => {
				localStorage.setItem(
					'Authuser',
					JSON.stringify(this.form.value)
				);
				localStorage.setItem('Authobj', JSON.stringify(data));
				localStorage.setItem('token', data.token);
				this._router.navigate(['/scrumboard', data['project_id']]);
			},
			(error) => {
				console.log('Error!', error);
				this.feedback = 'Invalid login credentials';
			}
		);
	}
}
