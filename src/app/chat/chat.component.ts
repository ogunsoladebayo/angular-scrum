import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { WebsocketService } from '../websocket.service';
import { Scrumuser } from '../scrumuser';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit {
	WS_URL: string =
		'wss://9oaktw3efg.execute-api.us-east-2.amazonaws.com/test';
	websocketConnection: WebSocket;
	messages: Array<object> = [];
	chat_text: String;
	logincred: any;
	messageFromServer: String;
	wsSubscription: Subscription;
	status: any;
	socket: WebSocket;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private wsService: WebsocketService
	) {
		this.websocketConnection = new WebSocket(this.WS_URL);
		// console.log('connect', this.websocketConnection);
	}

	ngOnInit() {
		this.websocketConnection.onopen = (event) => {
			const context = { action: 'getMessages' };
			this.websocketConnection.send(JSON.stringify(context));
		};
		this.websocketConnection.onmessage = async (event) => {
			let data = await JSON.parse(event.data);
			if (data['messages'] !== undefined) {
				data['messages'].forEach((message: object) => {
					this.messages.push(
						message['body'] ? message['body'] : message
					);
				});
			}
			var log = $('#chatlog');
			log.animate(
				{
					scrollTop: log.prop('scrollHeight'),
				},
				500
			);
		};
	}
	scrumLoginUserModel = new Scrumuser('', '', '', '', '');

	onClick() {
		let project_id = JSON.parse(localStorage.getItem('Authobj'));
		this._router.navigate(['/scrumboard/', project_id.project_id]);
	}

	getUsername() {
		this.logincred = JSON.parse(localStorage.getItem('Authobj'));
		return this.logincred.name;
	}

	getCurrentTime() {
		return new Date()
			.toLocaleTimeString()
			.replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, '$1$3');
	}

	sendMessage() {
		var log = $('#chatlog');
		log.animate(
			{
				scrollTop: log.prop('scrollHeight'),
			},
			500
		);
		if (this.chat_text !== '') {
			const context = {
				action: 'sendMessage',
				username: this.getUsername(),
				content: this.chat_text,
				timestamp: this.getCurrentTime(),
			};

			this.websocketConnection.send(JSON.stringify(context));
			this.chat_text = '';
		}
	}

	ngOnDestroy() {
		this.websocketConnection.close();
	}
}
