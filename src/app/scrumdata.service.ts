import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Scrumuser } from './scrumuser';
import { Creategoal } from './goals';

@Injectable({
	providedIn: 'root',
})
export class ScrumdataService {
	token: any;
	logincred: any;
	constructor(private _http: HttpClient) {}
	_url = 'https://liveapi.chatscrum.com/scrum/api/scrumusers/';
	_loginUrl = 'https://liveapi.chatscrum.com/scrum/api-token-auth/';
	_scrumProjectUrl = 'https://liveapi.chatscrum.com/scrum/api/scrumprojects/';
	_updateTaskUrl = 'https://liveapi.chatscrum.com/scrum/api/scrumgoals/';
	_changerole = 'https://liveapi.chatscrum.com/scrum/api/scrumprojectroles/';
	_sprintUrl = 'https://stageapi.chatscrum.com/scrum/api/scrumsprint/';

	public httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
	};

	signup(user: Scrumuser) {
		return this._http.post<any>(
			this._url,
			{
				email: user['email'],
				password: user['password'],
				full_name: user['fullname'],
				usertype: user['type'],
			},
			this.httpOptions
		);
	}
	login(user: { [x: string]: any }) {
		return this._http.post<any>(
			this._loginUrl,
			{
				username: user['email'],
				password: user['password'],
				project: user['projname'],
			},
			this.httpOptions
		);
	}

	loggedIn() {
		return !!localStorage.getItem('token');
	}
	allProjectGoals(project_id) {
		return this._http.get<any>(
			this._scrumProjectUrl + project_id,
			this.httpOptions
		);
	}
	updateTask(goal_id): Observable<any> {
		this.token = localStorage.getItem('token');
		this.logincred = JSON.parse(localStorage.getItem('Authuser'));
		this.logincred = btoa(
			`${this.logincred.email}:${this.logincred.password}`
		);
		return this._http.patch(
			this._updateTaskUrl + goal_id.id + '/',
			{ status: goal_id.status },
			{
				headers: new HttpHeaders().set(
					'Authorization',
					`Basic ${this.logincred}==`
				),
			}
		);
	}
	creategoal(user: Creategoal): Observable<any> {
		this.logincred = JSON.parse(localStorage.getItem('Authuser'));
		this.logincred = btoa(
			`${this.logincred.email}:${this.logincred.password}`
		);
		return this._http.post<any>(
			this._updateTaskUrl,
			{
				name: user['name'],
				project_id: user['projectid'],
				user: user['roleid'],
			},
			{
				headers: new HttpHeaders()
					.set('Authorization', `Basic ${this.logincred}==`)
					.append('Content-Type', 'application/json'),
			}
		);
	}
	create_project(user: Scrumuser) {
		this.logincred = JSON.parse(localStorage.getItem('Authuser'));
		this.logincred = btoa(
			`${this.logincred.email}:${this.logincred.password}`
		);
		return this._http.post<any>(
			this._url,
			{
				email: user['email'],
				password: user['password'],
				full_name: user['fullname'],
				usertype: user['type'],
				projname: user['project_name'],
			},
			{
				headers: new HttpHeaders()
					.set('Authorization', `Basic ${this.logincred}==`)
					.append('Content-Type', 'application/json'),
			}
		);
	}
	changerole(user: Scrumuser): Observable<any> {
		this.token = localStorage.getItem('token');
		this.logincred = JSON.parse(localStorage.getItem('Authuser'));
		this.logincred = btoa(
			`${this.logincred.email}:${this.logincred.password}`
		);
		return this._http.patch(
			this._changerole + user['password'] + '/',
			{ role: user['type'] },
			{
				headers: new HttpHeaders().set(
					'Authorization',
					`Basic ${this.logincred}==`
				),
			}
		);
	}
	createSprint(user): Observable<any> {
		// this.token = this.getUser().token;
		this.logincred = JSON.parse(localStorage.getItem('Authuser'));
		this.logincred = btoa(
			`${this.logincred.email}:${this.logincred.password}`
		);
		return this._http.post(
			this._sprintUrl + '?' + 'goal_project_id=' + user,
			{ project_id: user },
			{
				headers: new HttpHeaders()
					.set('Authorization', `Basic ${this.logincred}==`)
					.append('Content-Type', 'application/json'),
			}
		);
	}
}
