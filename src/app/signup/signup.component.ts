import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ScrumdataService } from '../scrumdata.service';
import { Scrumuser } from '../scrumuser';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
	form: FormGroup;
	submitted = false;
	Usertype: any = ['Developer', 'Owner'];

	ngOnInit() {
		this.submitted = true;
		this.form = this.fb.group({
			email: ['', [Validators.required, Validators.email]],
			fullname: ['', Validators.required],
			password: [
				'',
				[
					Validators.required,
					Validators.minLength(8),
					Validators.pattern(
						/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*([^a-zA-Z\d\s])).{8,}$/
					),
				],
			],
			usertype: ['', Validators.required],
		});
	}
	scrumUserModel = new Scrumuser('', '', '', '', '');
	feedback = '';
	constructor(
		private fb: FormBuilder,
		private _scrumdataService: ScrumdataService
	) {}

	// convenience getter for easy access to form fields
	get f() {
		return this.form.controls;
	}
	validate() {
		this.submitted = true;
	}

	formSubmit() {
		this.submitted = true;
		// stop here if form is invalid
		if (this.form.invalid) {
			return;
		}
		this._scrumdataService.signup(this.scrumUserModel).subscribe((data) => {
			console.log('Success', data);
			this.feedback = 'Your account was created successfully';
		});

		console.log(this.scrumUserModel);
		// display form values on success
		// alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.form.value, null, 4));
	}
	onReset() {
		this.submitted = false;
		this.form.reset();
	}
}
