import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ScrumboardComponent } from './scrumboard/scrumboard.component';
import { HomepageComponent } from './homepage/homepage.component';
import { CreateprojectComponent } from './createproject/createproject.component';
import { ChangeroleComponent } from './changerole/changerole.component';
import { CreategoalComponent } from './creategoal/creategoal.component';
import { ChatComponent } from './chat/chat.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
	{ path: '', component: HomepageComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'login', component: LoginComponent },
	{
		path: 'scrumboard/:project_id',
		component: ScrumboardComponent,
		canActivate: [AuthGuard],
	},
	{ path: 'chat', component: ChatComponent },
	{ path: 'createproject/:project_id', component: CreateprojectComponent },
	{ path: 'creategoal/:goal_id', component: CreategoalComponent },
	{ path: 'changerole', component: ChangeroleComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
